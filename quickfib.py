"""Print the nth Fibonacci number in O(log(n)) time"""

import sys

MATRIX = [[1,1],[1,0]]
IDENTITY = [[1,0],[0,1]]

def matmult(m, n):
	return [   [ sum([m[i][k] * n[k][j] for k in range(2)]) for i in range(2) ] for j in range(2)  ]

def quickfib(n):
	binary = bin(n)[:1:-1]
	current = MATRIX
	total = IDENTITY
	for i in binary:
		if i == '1':
			total = matmult(total, current)
		current = matmult(current, current)
	return total[0][1]

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print(quickfib(10000))
	else:
		print(quickfib(int(sys.argv[1])))
